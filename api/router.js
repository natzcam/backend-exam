const Authentication = require("./controllers/authentication");
const Posts = require("./controllers/posts");
const Comments = require("./controllers/comments");
const passportService = require("./services/passport");
const passport = require("passport");

const requireAuth = passport.authenticate("jwt", { session: false });

const requireSignin = passport.authenticate("local", { session: false });

module.exports = function(app) {
  app.get("/", function(req, res) {
    res.send({ hi: "there" });
  });

  app.post("/api/register", Authentication.signup);

  app.post("/api/login", requireSignin, Authentication.signin);

  app.get("/api/posts", Posts.getAll);

  app.get("/api/posts/:slug", Posts.find);

  app.get("/api/posts/:slug/comments", Comments.getAllByPost);

  app.post("/api/posts", requireAuth, Posts.create);

  app.post("/api/posts/:slug/comments", requireAuth, Comments.create);
};

const jwt = require("jwt-simple");
const User = require("../models/user");
const config = require("../config");

function tokenForUser(user) {
  const timestamp = new Date().getTime();

  //jwt payload object parameters
  //sub = subject
  //iat = issued at time
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  //User has already had their email and password authorized
  //We just need to give them a token

  res.json({ token: tokenForUser(req.user), token_type: "Bearer" });
};

exports.signup = function(req, res, next) {
  console.log(req.body);
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const password_confirmation = req.body.password_confirmation;

  if (!name || !password_confirmation || !email || !password) {
    return res.status(422).json({
      message: "The given data was invalid"
    });
  }

  if (password != password_confirmation) {
    return res.status(422).json({
      message: "The given data was invalid",
      errors: {
        password: ["Password confirmation does not match"]
      }
    });
  }

  //See if a user with the given email exists
  User.findOne({ email: email }, function(err, existingUser) {
    if (err) {
      return next(err);
    }

    //If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).json({
        message: "The given data was invalid",
        errors: {
          email: ["The email is already taken"]
        }
      });
    }

    //If a user with the email does NOT exist, create and save user record
    const user = new User({
      name: name,
      email: email,
      password: password
    });

    user.save(function(err, data) {
      if (err) {
        return next(err);
      }

      //Respond to request indicating the user was created
      res.json({id: data._id, name: name, email: email });
    });
  });
};

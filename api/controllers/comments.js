const Comment = require("../models/comment");
const Post = require("../models/post");
const slug = require("slug");

exports.getAllByPost = function(req, res, next) {
  if (!req.params.slug) {
    res.status(400).json({ message: "No slug" });
  }

  Post.findOne({ slug: req.params.slug }, function(err, post) {
    if (err) {
      return next(err);
    }

    if (!post) {
      res.status(404).json({
        message: "No query results for model [App\\Post]."
      });
    }

    Comment.find({ parent_id: post._id }, function(err, comments) {
      if (err) {
        return next(err);
      }

      comments = comments.map(function(comment) {
        return {
          id: comment._id,
          body: comment.body,
          creator_id: comment.creator_id,
          parent_id: comment.parent_id
        };
      });

      res.json({
        data: comments
      });
    });
  });
};

exports.create = function(req, res, next) {
  const body = req.body.body;

  if (!req.params.slug) {
    res.status(400).json({ message: "No slug" });
  }

  if (!body) {
    return res.status(422).json({
      message: "The given data was invalid"
    });
  }

  Post.findOne({ slug: req.params.slug }, function(err, post) {
    if (err) {
      return next(err);
    }

    if (!post) {
      res.status(404).json({
        message: "No query results for model [App\\Post]."
      });
    }

    console.log('comment create', post)

    const comment = new Comment({
      body: body,
      parent_id: post._id,
      creator_id: req.user._id
    });

    comment.save(function(err, data) {
      if (err) {
        return next(err);
      }

      data.id = data._id;
      res.status(201).json(data);
    });
  });
};

// exports.find = function(req, res, next) {
//   console.log(req.params);

//   if (!req.params.slug) {
//     res.status(400).json({ message: "No slug" });
//   }

//   Post.find({ slug: req.params.slug }, function(err, post) {
//     if (err) {
//       return next(err);
//     }

//     if (!post) {
//       res.status(404).json({
//         message: "No query results for model [App\\Post]."
//       });
//     }

//     console.log("found post", post);

//     res.json({
//       data: {
//         id: post[0]._id,
//         title: post[0].title,
//         content: post[0].content,
//         slug: post[0].slug
//       }
//     });
//   });
// };

const Post = require("../models/post");
const slug = require("slug");

exports.getAll = function(req, res, next) {
  console.log(req.body);
  const limit = 15;
  let page = 1;
  if (req.query.page) {
    page = req.query.page;
  }

  Post.find(
    {},
    null,
    {
      skip: (page - 1) * limit,
      limit: limit
    },
    function(err, posts) {
      if (err) {
        return next(err);
      }

      posts = posts.map(function(post) {
        return {
          id: post._id,
          title: post.title,
          content: post.content,
          image: post.image,
          slug: post.slug,
          user_id: post.user_id
        };
      });

      res.json({
        data: posts
      });
    }
  );
};

exports.create = function(req, res, next) {
  console.log("user", req.user);

  const title = req.body.title;
  const content = req.body.content;
  const image = req.body.image;

  if (!title || !content) {
    return res.status(422).json({
      message: "The given data was invalid"
    });
  }

  const post = new Post({
    title: title,
    content: content,
    image: image,
    slug: slug(title, { lower: true }),
    user_id: req.user._id
  });

  post.save(function(err, data) {
    if (err) {
      return next(err);
    }

    data.id = data._id;
    res.status(201).json(data);
  });
};

exports.find = function(req, res, next) {
  console.log(req.params);

  if (!req.params.slug) {
    res.status(400).json({ message: "No slug" });
  }

  Post.findOne({ slug: req.params.slug }, function(err, post) {
    if (err) {
      return next(err);
    }

    if (!post) {
      res.status(404).json({
        message: "No query results for model [App\\Post]."
      });
    }

    console.log("found post", post);

    res.json({
      data: {
        id: post._id,
        title: post.title,
        content: post.content,
        image: post.image,
        slug: post.slug,
        user_id: post.user_id
      }
    });
  });
};

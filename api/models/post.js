const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: String,
    content: String,
    slug: String,
    user_id: String
});

//Create the post class
const PostClass = mongoose.model('post', postSchema);

//Export the model
module.exports = PostClass;

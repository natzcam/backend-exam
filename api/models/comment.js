const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  body: String,
  creator_id: String,
  parent_id: String
});

//Create the comment class
const CommentClass = mongoose.model("comment", commentSchema);

//Export the model
module.exports = CommentClass;
